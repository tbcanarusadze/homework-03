package com.example.plus_minus

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }


    private fun init() {
        plusButton.setOnClickListener{
            changeNumber("+")
        }
        minusButton.setOnClickListener {
            changeNumber("-")
        }

//        plusButton.setOnLongClickListener {
//            changeNumber("+")
//            true
//        }
//        minusButton.setOnLongClickListener {
//            changeNumber("-")
//            true
//        }

//        onclick function:
//        plusButton.setOnClickListener(this)
//        minusButton.setOnClickListener(this)

    }

    private fun changeNumber(operator: String) {
        var number = numberTextView.text.toString().toInt()
        if (operator == "+" && number != 9) {
            number++
        } else if (operator == "-" && number != 0) {
            number--
        }
        numberTextView.text = number.toString()
    }

//    second option:
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.minusButton -> changeNumber("-")
            R.id.plusButton -> changeNumber("+")
        }
    }


}
